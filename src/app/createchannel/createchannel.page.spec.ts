import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatechannelPage } from './createchannel.page';

describe('CreatechannelPage', () => {
  let component: CreatechannelPage;
  let fixture: ComponentFixture<CreatechannelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatechannelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatechannelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

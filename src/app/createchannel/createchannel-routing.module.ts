import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatechannelPage } from './createchannel.page';

const routes: Routes = [
  {
    path: '',
    component: CreatechannelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatechannelPageRoutingModule {}

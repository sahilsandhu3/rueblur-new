import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatechannelPageRoutingModule } from './createchannel-routing.module';

import { CreatechannelPage } from './createchannel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatechannelPageRoutingModule
  ],
  declarations: [CreatechannelPage]
})
export class CreatechannelPageModule {}

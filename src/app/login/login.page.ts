import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { UserApiService} from '../services/user-api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  passwordType = "password";
  passwordVisible:boolean = false;
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 char. long.' }
    ]
  };
  constructor(private route:Router, private userApi:UserApiService, private storage: Storage, public nav:NavController) {
    this.loginForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
   }

  ngOnInit() {
  }

  showPassword(value){
    console.log(value);
    if(value == 'show'){
      this.passwordType = 'text'
        this.passwordVisible = true
    }else{
      this.passwordType = 'password',
      this.passwordVisible = false;
    }
}

  signIn(){
    var obj ={
      'email':this.loginForm.controls.email.value,
      'password':this.loginForm.controls.password.value
    }
    this.userApi.loginApi(obj).subscribe((res =>{
          if(res){
            this.storage.set('userDetail',res)
            window.localStorage.setItem('token', res['token']);
            this.nav.navigateForward('tabs/home')
          }

    }), (error =>{
          console.log(error);
    }))
  }
}

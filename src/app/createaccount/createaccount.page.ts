import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonMethodService } from '../services/common-method.service';

@Component({
  selector: 'app-createaccount',
  templateUrl: './createaccount.page.html',
  styleUrls: ['./createaccount.page.scss'],
})
export class CreateaccountPage implements OnInit {
  registerForm: FormGroup;
  countryNameList:any;
  data;
  constructor(private route:Router, private commonMethod:CommonMethodService, public activeRoute: ActivatedRoute) { 

      this.registerForm = new FormGroup({
        'first_name':new FormControl('', Validators.required),
        'last_name':new FormControl('', Validators.required),
        'username':new FormControl('', Validators.required),
        'about':new FormControl('', Validators.required),
        'country':new FormControl('', Validators.required),
        'state':new FormControl('', Validators.required),
        'city':new FormControl('', Validators.required),
        'school':new FormControl('', Validators.required),
        'relationship':new FormControl('', Validators.required),
        'occupation':new FormControl('', Validators.required),
      })
  }

  ngOnInit() {
  }

  ionViewWillEnter() {  
          this.commonMethod.getCountryList().subscribe(res=>{
                console.log(res);
                this.countryNameList = res;
          })
          this.data = JSON.parse(this.activeRoute.snapshot.parent.paramMap.get('data')); 
  }

  registerAccount(){
    var registerJson = {}
    registerJson = this.data;
    registerJson['first_name'] =this.registerForm.controls.first_name.value,
    registerJson['last_name'] = this.registerForm.controls.last_name.value,
    registerJson['username'] = this.registerForm.controls.username.value,
    registerJson['about'] = this.registerForm.controls.about.value,
    registerJson['country'] = this.registerForm.controls.about.value,
    registerJson['state'] = this.registerForm.controls.state.value,
    registerJson['city'] = this.registerForm.controls.city.value,
    registerJson['school']= this.registerForm.controls.school.value,
    registerJson['relationship'] = this.registerForm.controls.relationship.value,
    registerJson['occupation'] = this.registerForm.controls.occupation.value
    registerJson['profile_pic'] = null;
      this.route.navigate(['category', JSON.stringify(registerJson)]);
    console.log(registerJson);
  }

}

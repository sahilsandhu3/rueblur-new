import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignupasPage } from './signupas.page';

describe('SignupasPage', () => {
  let component: SignupasPage;
  let fixture: ComponentFixture<SignupasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { CommonMethodService } from '../services/common-method.service';

@Component({
  selector: 'app-signupas',
  templateUrl: './signupas.page.html',
  styleUrls: ['./signupas.page.scss'],
})
export class SignupasPage implements OnInit {

  constructor(private route:Router, private commonMethod:CommonMethodService, private storage: Storage) { }

  ngOnInit() {
  }

  signUpAs(value){
    var obj ={
      'account_detail':value
    }    
          console.log(obj);
        this.route.navigate(['signup/', JSON.stringify(obj)])
  }

}

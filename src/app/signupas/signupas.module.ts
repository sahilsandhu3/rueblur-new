import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignupasPageRoutingModule } from './signupas-routing.module';

import { SignupasPage } from './signupas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignupasPageRoutingModule
  ],
  declarations: [SignupasPage]
})
export class SignupasPageModule {}

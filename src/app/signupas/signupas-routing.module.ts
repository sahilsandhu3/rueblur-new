import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupasPage } from './signupas.page';

const routes: Routes = [
  {
    path: '',
    component: SignupasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignupasPageRoutingModule {}

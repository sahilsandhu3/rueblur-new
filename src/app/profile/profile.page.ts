import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  slideOpts = {
    slidesPerView: 2,
    spaceBetween: 5 
  };

  constructor() { }

  ngOnInit() {
  }

}

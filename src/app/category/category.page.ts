import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from '../services/user-api.service';
import { Storage } from '@ionic/storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoriesList } from '../categoryList';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
  data;
  favCategory
  categoryForm:FormGroup;
  categories:any

  constructor(private route:Router, private userApi:UserApiService, public activeRoute: ActivatedRoute, private storage: Storage, public listCategory:CategoriesList, public nav:NavController) {   
    this.categoryForm = new FormGroup({
        'favCategory': new FormControl('', Validators.required)
      })
  }
  
  ngOnInit() {
    this.categories = this.listCategory.CATEGORIES;
    console.log(this.categories);
    
  }

  ionViewWillEnter() { 
    this.data = JSON.parse(this.activeRoute.snapshot.parent.paramMap.get('data')); 
    console.log(this.data);
}

categorySubmit(){
    this.data['fav_category'] = this.categoryForm.controls.favCategory.value;
    this.userApi.registerApi(this.data).subscribe((res)=>{
            
      console.log(res);
        if(res['status_code'] === 200){
              this.generateToken(); 
        }

    }, (err)=>{
          console.log(err);
    })

}

generateToken(){
  var obj ={
    'email':this.data['email'],
    'password':this.data['password']
  }
    this.userApi.generateTokenApi(obj).subscribe((res)=>{
            if(res){
              this.data['user_id'] = res['user_id'],
              this.data['token'] = res['token']
              this.getProfile();
            }
    }, (error)=>{
          console.log(error);
    })
}

getProfile(){
      this.userApi.getProfileApi(this.data['token']).subscribe((res =>{
            console.log(res);
                
            if(res['status'] == 'ok'){
                  this.updateToken()      
            }

      }), (error =>{
              console.log(error)
      }))
}

updateToken(){
      var obj = {
            'deviceData':[{
                'logindate': "2020-12-09T08:30:54.305Z",
                'model': null,
                'offset': -330,
                'platform': null,
                'uuid': null,
                'version': null
            }],
            'status':'login',
            'userid':this.data['user_id']

      }


      this.userApi.updateTokenApi(obj).subscribe((res =>{
          console.log(res);
            this.getMyNotCount()

      }), (error =>{
              console.log(error);
      }))
}

getMyNotCount(){
      this.userApi.getMyNotCountApi({'token':this.data['token']}).subscribe((res =>{
            if(res['status'] == 'ok'){
              this.storage.set('userDetail',this.data)
              window.localStorage.setItem('token', this.data['token']);
              this.nav.navigateForward('tabs/home')
            }
      }), (error =>{

      }))
}


}

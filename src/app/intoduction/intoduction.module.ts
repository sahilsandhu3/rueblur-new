import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IntoductionPageRoutingModule } from './intoduction-routing.module';

import { IntoductionPage } from './intoduction.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IntoductionPageRoutingModule
  ],
  declarations: [IntoductionPage]
})
export class IntoductionPageModule {}

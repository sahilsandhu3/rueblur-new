import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntoductionPage } from './intoduction.page';

const routes: Routes = [
  {
    path: '',
    component: IntoductionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IntoductionPageRoutingModule {}

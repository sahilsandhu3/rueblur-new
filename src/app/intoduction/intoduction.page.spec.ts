import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IntoductionPage } from './intoduction.page';

describe('IntoductionPage', () => {
  let component: IntoductionPage;
  let fixture: ComponentFixture<IntoductionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntoductionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IntoductionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatepostnewPage } from './createpostnew.page';

describe('CreatepostnewPage', () => {
  let component: CreatepostnewPage;
  let fixture: ComponentFixture<CreatepostnewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatepostnewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatepostnewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

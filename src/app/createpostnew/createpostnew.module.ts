import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatepostnewPageRoutingModule } from './createpostnew-routing.module';

import { CreatepostnewPage } from './createpostnew.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatepostnewPageRoutingModule
  ],
  declarations: [CreatepostnewPage]
})
export class CreatepostnewPageModule {}

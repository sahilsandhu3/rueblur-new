import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-createpostnew',
  templateUrl: './createpostnew.page.html',
  styleUrls: ['./createpostnew.page.scss'],
})
export class CreatepostnewPage implements OnInit {

  constructor( public nav:NavController) { }

  ngOnInit() {
  }

  back(){
    this.nav.back()
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatepostnewPage } from './createpostnew.page';

const routes: Routes = [
  {
    path: '',
    component: CreatepostnewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatepostnewPageRoutingModule {}

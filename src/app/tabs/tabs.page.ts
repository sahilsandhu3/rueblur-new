import { Component } from '@angular/core';
import { Platform ,NavController,MenuController} from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    public menuctrl:MenuController
  ) {}
  toggleMenu(){
    this.menuctrl.toggle()
  }
}

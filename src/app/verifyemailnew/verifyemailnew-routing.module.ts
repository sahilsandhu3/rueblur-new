import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerifyemailnewPage } from './verifyemailnew.page';

const routes: Routes = [
  {
    path: '',
    component: VerifyemailnewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerifyemailnewPageRoutingModule {}

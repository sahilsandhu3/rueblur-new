import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerifyemailnewPageRoutingModule } from './verifyemailnew-routing.module';

import { VerifyemailnewPage } from './verifyemailnew.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerifyemailnewPageRoutingModule
  ],
  declarations: [VerifyemailnewPage]
})
export class VerifyemailnewPageModule {}

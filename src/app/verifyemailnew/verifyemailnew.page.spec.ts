import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VerifyemailnewPage } from './verifyemailnew.page';

describe('VerifyemailnewPage', () => {
  let component: VerifyemailnewPage;
  let fixture: ComponentFixture<VerifyemailnewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyemailnewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VerifyemailnewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';
import { CommonMethodService } from '../services/common-method.service';
import { UserApiService } from '../services/user-api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  account_detail:string;
  signUpForm: FormGroup;  
  passwordType = "password";
  passwordVisible:boolean = false;
  constructor(private route:Router, private commonMethod:CommonMethodService, private storage: Storage, private userApi:UserApiService, public activeRoute: ActivatedRoute){
    this.signUpForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
   }

  ngOnInit() {
  }

  showPassword(value){
      if(value == 'show'){
        this.passwordType = 'text'
          this.passwordVisible = true
      }else{
        this.passwordType = 'password',
        this.passwordVisible = false;
      }
  }

  ionViewWillEnter() { 
    var data
    data = JSON.parse(this.activeRoute.snapshot.parent.paramMap.get('data')); 
    console.log(data);
    this.account_detail = data['account_detail']
  }

  signUp(){
      this.userApi.sendOtp({'email':this.signUpForm.controls.email.value}).subscribe(res=>{
          if(res['status_code'] == 200){
            var obj ={
              'account_detail':this.account_detail,
              'email':this.signUpForm.controls.email.value,
              'password':this.signUpForm.controls.password.value,
              'passwordOtp': res['otp']
            }
            this.route.navigate(['verifyemail', JSON.stringify(obj)])
          } else{
            console.log('try again later')
          }
      })

      
  }

}

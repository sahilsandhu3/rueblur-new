import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MygalleryPage } from './mygallery.page';

describe('MygalleryPage', () => {
  let component: MygalleryPage;
  let fixture: ComponentFixture<MygalleryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MygalleryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MygalleryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

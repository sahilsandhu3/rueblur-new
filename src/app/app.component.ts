import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform ,NavController} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public nav:NavController,
    private storage: Storage
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.storage.get('userDetail').then(userInfo =>{

        if(userInfo === null){
          this.nav.navigateForward('login')
        }else{
        if(userInfo['token'] != ''){
          this.nav.navigateForward('tabs/home')
        }
        }
        let systemDark = window.matchMedia("(prefers-color-scheme: dark)");
       this.colorTest(systemDark);
    })

    });
  }

  onClick(event){
    let systemDark = window.matchMedia("(prefers-color-scheme: dark)");
    console.log(systemDark)
    systemDark.addListener(this.colorTest);
    if(event.detail.checked){
      document.body.setAttribute('class', 'dark');
    }
    else{
      document.body.setAttribute('class', 'light');
    }
  }

   colorTest(systemInitiatedDark) {
     console.log(systemInitiatedDark)
    if (systemInitiatedDark.matches) {
      document.body.setAttribute('class', 'dark');		
    } else {
      document.body.setAttribute('class', 'light');
    }
  }





}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChannellistPage } from './channellist.page';

const routes: Routes = [
  {
    path: '',
    component: ChannellistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChannellistPageRoutingModule {}

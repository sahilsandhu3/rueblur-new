import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChannellistPage } from './channellist.page';

describe('ChannellistPage', () => {
  let component: ChannellistPage;
  let fixture: ComponentFixture<ChannellistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannellistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChannellistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

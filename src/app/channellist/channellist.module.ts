import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChannellistPageRoutingModule } from './channellist-routing.module';

import { ChannellistPage } from './channellist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChannellistPageRoutingModule
  ],
  declarations: [ChannellistPage]
})
export class ChannellistPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReplycommentPage } from './replycomment.page';

const routes: Routes = [
  {
    path: '',
    component: ReplycommentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReplycommentPageRoutingModule {}

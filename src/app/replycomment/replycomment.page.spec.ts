import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReplycommentPage } from './replycomment.page';

describe('ReplycommentPage', () => {
  let component: ReplycommentPage;
  let fixture: ComponentFixture<ReplycommentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplycommentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReplycommentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

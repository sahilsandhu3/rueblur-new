import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReplycommentPageRoutingModule } from './replycomment-routing.module';

import { ReplycommentPage } from './replycomment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReplycommentPageRoutingModule
  ],
  declarations: [ReplycommentPage]
})
export class ReplycommentPageModule {}

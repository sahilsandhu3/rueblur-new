import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MychannelPage } from './mychannel.page';

const routes: Routes = [
  {
    path: '',
    component: MychannelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MychannelPageRoutingModule {}

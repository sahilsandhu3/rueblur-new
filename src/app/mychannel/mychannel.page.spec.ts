import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MychannelPage } from './mychannel.page';

describe('MychannelPage', () => {
  let component: MychannelPage;
  let fixture: ComponentFixture<MychannelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MychannelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MychannelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

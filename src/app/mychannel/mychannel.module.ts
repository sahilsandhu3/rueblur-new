import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MychannelPageRoutingModule } from './mychannel-routing.module';

import { MychannelPage } from './mychannel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MychannelPageRoutingModule
  ],
  declarations: [MychannelPage]
})
export class MychannelPageModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommonMethodService } from '../services/common-method.service';
import { UserApiService } from '../services/user-api.service';

@Component({
  selector: 'app-verifyemail',
  templateUrl: './verifyemail.page.html',
  styleUrls: ['./verifyemail.page.scss'],
})
export class VerifyemailPage implements OnInit {
  userEmailId:string = 'home5@yopmail.com';
  generatedOtp:any;
  constructor(private route:Router, private userApi:UserApiService, private commonMethod:CommonMethodService, public activeRoute: ActivatedRoute) { }
    data;
  ngOnInit() {
  }

  ionViewWillEnter() { 
    this.data = JSON.parse(this.activeRoute.snapshot.parent.paramMap.get('data')); 
    this.userEmailId = this.data['email']
    this.generatedOtp = this.data['passwordOtp']
  }

  verifyOtp(value){
    if(value.length === 4){
            if(this.generatedOtp === parseInt(value)){
              this.route.navigate(['createaccount', JSON.stringify(this.data)]);
        }else{
          console.log('password incorrect');
        }
    }
    
  }

  
}

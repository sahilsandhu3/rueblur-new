import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Createpost1Page } from './createpost1.page';

const routes: Routes = [
  {
    path: '',
    component: Createpost1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Createpost1PageRoutingModule {}

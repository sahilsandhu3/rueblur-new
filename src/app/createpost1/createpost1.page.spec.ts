import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Createpost1Page } from './createpost1.page';

describe('Createpost1Page', () => {
  let component: Createpost1Page;
  let fixture: ComponentFixture<Createpost1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Createpost1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Createpost1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Createpost1PageRoutingModule } from './createpost1-routing.module';

import { Createpost1Page } from './createpost1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Createpost1PageRoutingModule
  ],
  declarations: [Createpost1Page]
})
export class Createpost1PageModule {}

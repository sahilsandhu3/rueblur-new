import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonMethodService } from '../services/common-method.service';
import { UserApiService } from '../services/user-api.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  forgotForm:FormGroup;
  validation_messages = {
		userEmail: [
			{ type: "required", message: "Email is required." },
			{ type: "pattern", message: "Enter a valid email." }
		]
	};
  constructor(private userApi: UserApiService, private commonMethod: CommonMethodService) { 
    this.forgotForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(
          "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
        )
      ]))
    })
  }

  ngOnInit() {
  }

  resetPassword(){
      this.commonMethod.showLoader();
      this.userApi.forgotPasswordApi({'email':this.forgotForm.controls.email.value}).subscribe((res =>{
                this.commonMethod.dismissLoading()
                if(res['status'] == 'ok'){
                    this.commonMethod.presentAlert(res['msg'])
                }
      }), (err =>{
              this.commonMethod.dismissLoading()
              this.commonMethod.presentAlert(err.error['msg'])
      }))
  }

}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonMethodService } from './common-method.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(private commonMethod: CommonMethodService, private router: Router,  private storage: Storage) {}

  canActivate():boolean{
        if(window.localStorage.getItem('token') != null && window.localStorage.getItem('token') != '' && window.localStorage.getItem('token') != undefined){
              return true;
        }else{
              this.router.navigate(['/login'])
              return false;
        }
  } 
}

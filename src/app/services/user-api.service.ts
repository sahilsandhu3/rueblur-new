import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserApiService {
  loginUrl = 'http://rueblur.betaplanets.com/wp-json/jwt-auth/v1/token';
  verifyUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/verify_email';
  registerUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/register';
  tokenUrl = 'http://rueblur.betaplanets.com/wp-json/jwt-auth/v1/token';
  updateTokenUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/updateDeviceToken';
  profileUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/getProfile/';
  getCountUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/get_my_not_count';
  forgotPasswdUrl = 'http://rueblur.betaplanets.com/wp-json/mobileapi/v1/retrieve_password';
  constructor(
    private http: HttpClient,
  ) { }

  loginApi(obj) {
    return this.http.post(this.loginUrl, obj)
  }

  sendOtp(verifyObj){
    return this.http.post(this.verifyUrl, verifyObj)
  }

  registerApi(registerData){
    return this.http.post(this.registerUrl, registerData)
  }

  generateTokenApi(userInfo){
      return this.http.post(this.tokenUrl, userInfo)
  }

  updateTokenApi(userDeviceInfo){
    return this.http.post(this.updateTokenUrl, userDeviceInfo)
  }

  getMyNotCountApi(token){
    return this.http.post(this.getCountUrl, token);
  }

  getProfileApi(token){
    var url = this.profileUrl + '?token='+token
    return this.http.get(url);
  }
  forgotPasswordApi(userInfo){
    return this.http.post(this.forgotPasswdUrl, userInfo)
  }

}

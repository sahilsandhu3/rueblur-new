import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, } from '@angular/common/http';
import { AlertController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class CommonMethodService {
  private signUpDetailsSubject = new Subject<any>()
  countryUrl = "https://restcountries.eu/rest/v2/all";
  loading: any;
  constructor( private http: HttpClient,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private storage: Storage,
    ) {}

    // alert
    async presentAlert(msg) {
      let alert = await this.alertCtrl.create({
        // header: 'Alert',
        message: msg,
        buttons: [{
          text: 'OK',
          handler: () => {

          }
        }]
      });
  
      await alert.present();
    }



    // page loader.
  async dismissLoading() {
    await this.loading.dismiss();
  }

  async showLoader(msg: string = '') {
    if (msg == '') {
      msg = 'Please wait...';
    }
    this.loading = await this.loadingCtrl.create({ message: msg });
    await this.loading.present();
  }


  getCountryList(){
    return this.http.get(this.countryUrl)
  }
  
  validateToken() {
    var data;
    this.storage.get('userDetail').then(userInfo =>{
              console.log(userInfo);
            if(userInfo === null){
              return false;
            }else{
              console.log('inside')
              data = JSON.parse(userInfo)
                      if(data['token'] != ''){
                          return true
                      }
            }
          })
  }


}

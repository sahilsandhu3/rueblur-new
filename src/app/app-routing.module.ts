import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './services/authentication.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
    // canActivate: [AuthenticationGuard] 
  },
  {
    path: 'signup/:data',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'signupas',
    loadChildren: () => import('./signupas/signupas.module').then( m => m.SignupasPageModule)
  },
  {
    path: 'verifyemail/:data',
    loadChildren: () => import('./verifyemail/verifyemail.module').then( m => m.VerifyemailPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'explore',
    loadChildren: () => import('./explore/explore.module').then( m => m.ExplorePageModule)
  },
  {
    path: 'media',
    loadChildren: () => import('./media/media.module').then( m => m.MediaPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthenticationGuard] 
  },
  {
    path: 'channellist',
    loadChildren: () => import('./channellist/channellist.module').then( m => m.ChannellistPageModule)
  },
  {
    path: 'messagelist',
    loadChildren: () => import('./messagelist/messagelist.module').then( m => m.MessagelistPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'rank',
    loadChildren: () => import('./rank/rank.module').then( m => m.RankPageModule)
  },
  {
    path: 'createpost',
    loadChildren: () => import('./createpost/createpost.module').then( m => m.CreatepostPageModule)
  },
  {
    path: 'mygallery',
    loadChildren: () => import('./mygallery/mygallery.module').then( m => m.MygalleryPageModule)
  },
  {
    path: 'mypost',
    loadChildren: () => import('./mypost/mypost.module').then( m => m.MypostPageModule)
  },
  {
    path: 'savedpost',
    loadChildren: () => import('./savedpost/savedpost.module').then( m => m.SavedpostPageModule)
  },
  {
    path: 'createchannel',
    loadChildren: () => import('./createchannel/createchannel.module').then( m => m.CreatechannelPageModule)
  },
  {
    path: 'mychannel',
    loadChildren: () => import('./mychannel/mychannel.module').then( m => m.MychannelPageModule),
    canActivate: [AuthenticationGuard]   
  },
  {
    path: 'replycomment',
    loadChildren: () => import('./replycomment/replycomment.module').then( m => m.ReplycommentPageModule)
  },
  {
    path: 'createaccount/:data',
    loadChildren: () => import('./createaccount/createaccount.module').then( m => m.CreateaccountPageModule)
  },
  {
    path: 'publicprofile',
    loadChildren: () => import('./publicprofile/publicprofile.module').then( m => m.PublicprofilePageModule)
  },
  {
    path: 'singlepost',
    loadChildren: () => import('./singlepost/singlepost.module').then( m => m.SinglepostPageModule),
    canActivate: [AuthenticationGuard] 
  },
  {
    path: 'createpost1',
    loadChildren: () => import('./createpost1/createpost1.module').then( m => m.Createpost1PageModule)
  },
  {
    path: 'subscribe',
    loadChildren: () => import('./subscribe/subscribe.module').then( m => m.SubscribePageModule)
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  {
    path: 'category/:data',
    loadChildren: () => import('./category/category.module').then( m => m.CategoryPageModule)
  },
  {
    path: 'createpostnew',
    loadChildren: () => import('./createpostnew/createpostnew.module').then( m => m.CreatepostnewPageModule),
    canActivate: [AuthenticationGuard] 
  },
  {
    path: 'verifypassword',
    loadChildren: () => import('./verifypassword/verifypassword.module').then( m => m.VerifypasswordPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

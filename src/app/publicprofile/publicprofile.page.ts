import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-publicprofile',
  templateUrl: './publicprofile.page.html',
  styleUrls: ['./publicprofile.page.scss'],
})
export class PublicprofilePage implements OnInit {
  slideOpts = {
    slidesPerView: 2,
    spaceBetween: 5 
  };

  
  constructor() { }

  ngOnInit() {
  }

}
